#include <vector>
#include <iostream>


int main() {
    std::vector<int> vec = {1, 2, 3, 4};
    std::vector<int>::iterator begin_of_vector = vec.begin();
    *begin_of_vector = 3;
    begin_of_vector = begin_of_vector + 2;
    std::cout << *vec.end() << std::endl;

    for(int i = 0; i != vec.size(); i++)
        std::cout << vec.at(i) << std::endl;

    for(auto it = vec.rbegin(); it != vec.rend(); it++) {
        //*it = 0;
        std::cout << *it << " ";
    }   
    std::cout << "\n";

    for(int element : vec) {
        std::cout << element << " ";
    }   
    std::cout << "\n";


}