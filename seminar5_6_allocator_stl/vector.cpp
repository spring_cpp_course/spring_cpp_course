#include <vector>
#include <iostream>
#include <chrono>

std::vector<int> createVector(size_t size){
    std::vector<int> result;
    for(size_t i = 0; i < size; i++){
        int val = i*i;
        result.push_back(val);
        //std::cout << "Size = " << result.size() << std::endl;
        //std::cout <<"Capacity=" << result.capacity() << std::endl;
    }

    return result;
}

std::vector<int> createVectorAllocate(size_t size){
    std::vector<int> result;
    result.reserve(size);
    for(size_t i = 0; i < size; i++){
        int val = i*i;
        result.push_back(val);
        //std::cout << "Size = " << result.size() << std::endl;
        //std::cout <<"Capacity=" << result.capacity() << std::endl;
    }

    return result;
}

template<typename T>
void printVector(std::vector<T>& vec){
    for(T element : vec) {
        std::cout << element << " ";
    }  
    std::cout << "\n";
}

int main() {

    long int size = 10;
    auto start = std::chrono::high_resolution_clock::now();
    std::vector<int> vec1 = createVector(size);
    printVector(vec1);

    auto it_elemnt = vec1.begin();
    for(auto it = vec1.begin(); it != vec1.end(); it++)
        if(*it == 9)
            it_elemnt = it;
    vec1.erase(it_elemnt);
    vec1.insert(++it_elemnt, 10);

    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "Default vector allocate time: " << duration.count() << "ms.\n";
    printVector(vec1);
    vec1.clear();
    std::cout <<"Capacity=" << vec1.capacity() << std::endl;
    vec1.shrink_to_fit();
    std::cout <<"Capacity=" << vec1.capacity() << std::endl;

    start = std::chrono::high_resolution_clock::now();
    std::vector<int> vec2 = createVectorAllocate(size);
    stop = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "Allocated vector allocate time: " << duration.count() << "ms.\n";
}