#include <iostream>

class MyClass {
public:
    MyClass() {
        std::cout << "Construct\n";
    }
    ~MyClass() {
        std::cout << "Destroy\n";
    }
};

int main() 
{
    std::cout << "Start program\n";
    {
        MyClass* c = new MyClass();
    }
    std::cout << "End program\n";
          delete c;
}