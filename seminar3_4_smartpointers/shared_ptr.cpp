
#include <iostream>
#include <memory>

class MyClass {
public:
    MyClass() {
        std::cout << "Construct\n";
    }
    ~MyClass() {
        std::cout << "Destroy\n";
    }
    void something() {
        std::cout << "Do something\n";
    }
    
    int a = 0;
};

void foo(MyClass* cls) {
    cls->a = 4;
}


int main() {
    // std::unique_ptr<std::unique_ptr<MyClass>> arrPtr = std::make_unique<std::unique_ptr<MyClass>>(10);
    // for(int i = 0; i < 9; i++)
    //     *(arrPtr + i) = std::make_unique<MyClass>();

    MyClass* c  = new MyClass();
    std::shared_ptr<MyClass> shrPtr1(c);
    if(shrPtr1 != nullptr){
        //Do something
    }
    auto shrPtr3 = std::make_shared<MyClass>();
    {
        std::shared_ptr<MyClass> shrPtr2(shrPtr1);
        std::cout << "End of block\n";
    }
    std::cout << "End of program\n";
}