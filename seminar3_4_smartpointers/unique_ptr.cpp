
#include <iostream>
#include <memory>

class MyClass {
public:
    MyClass() {
        std::cout << "Construct\n";
    }
    ~MyClass() {
        std::cout << "Destroy\n";
    }
    void something() {
        std::cout << "Do something\n";
    }
    
    int a = 0;
};

void foo(MyClass* cls) {
    cls->a = 4;
}


int main() {
    std::unique_ptr<MyClass> ptrCls = std::make_unique<MyClass>(/*params*/);
    std::unique_ptr<MyClass> ptr;
    std::cout << ptrCls->a << std::endl;
    ptr = std::move(ptrCls);
    //std::cout << ptrCls->a << std::endl;
    ptr->a = 3;
    std::cout << ptr->a << std::endl;
    foo(ptr.get());
    std::cout << ptr->a << std::endl;
    std::cout << "End of program\n";
}