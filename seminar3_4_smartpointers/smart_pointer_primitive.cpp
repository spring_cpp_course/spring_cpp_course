
#include <iostream>
#include <memory>

class MyClass {
public:
    MyClass() {
        std::cout << "Construct\n";
    }
    ~MyClass() {
        std::cout << "Destroy\n";
    }
    void something() {
        std::cout << "Do something\n";
    }
    
    int a = 0;
};

// Класс нашего умного указателя
template <typename T>
class SmartPointer {
    T *m_obj;
public:
    // Отдаем ему во владение некий объект
    SmartPointer(T *obj)
        : m_obj(obj)
    { }
    SmartPointer(SmartPointer& s)
    {
        m_obj = s.m_obj;
        s.m_obj = nullptr;
    }
    // По выходу из области видимости этот объект мы уничтожим
    ~SmartPointer() {
        delete m_obj;
    }
    // Перегруженные операторы<
    // Селектор. Позволяет обращаться к данным типа T посредством "стрелочки"
    T* operator->() { return m_obj; }
    // С помощью такого оператора мы можем разыменовать указатель и получить ссылку на
    // объект, который он хранит
    T& operator* () { return *m_obj; }
};


int main() {
    // Отдаем myClass во владение умному указателю

    SmartPointer<MyClass> pMyClass(new MyClass(/*params*/));    
    SmartPointer<MyClass> pMyClass2(pMyClass);  
    int* a = new int[10];
    delete a;
    SmartPointer<int> array(new int[10]); 

    // Обращаемся к методу класса MyClass посредством селектора
    pMyClass2->something();    
    // Допустим, что для нашего класса есть функция вывода его в ostream
    // Эта функция одним из параметров обычно получает ссылку на объект,
    // который должен быть выведен на экран
    std::cout << (*pMyClass2).a << std::endl;    
    // по выходу из скоупа объект MyClass будет удален
}